package com.tessco.orderservice.controller;

import com.tessco.orderservice.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/orders")
@Api(value = "orders")
@SuppressWarnings("SpellCheckingInspection")
public class OrderController {
    private OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping(value = "/signalgroup", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.TEXT_PLAIN_VALUE)
    @ApiOperation(value = "Sends incoming request message to Amazon SQS Queue")
    public ResponseEntity<String> sendMessageToSqsQueue(@RequestBody String request) {
        return ResponseEntity.ok(orderService.sendMessageToSqsQueue(request));
    }

}

