package com.tessco.orderservice.service;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

import static com.tessco.orderservice.utility.Constants.SEND_MESSAGE_TO_OES_SUCCESS;


@Service
@SuppressWarnings("SpellCheckingInspection")
public class OrderService {

    @Value("${queue.url}")
    private String queueUrl;

    @Value("${queue.accesskey}")
    private String accessKey;

    @Value("${queue.secretkey}")
    private String secretKey;

    private AmazonSQS sqs;

    @PostConstruct
    public void init() {
        sqs = AmazonSQSClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey)))
                .withRegion(Regions.US_EAST_2)
                .build();
    }

    public String sendMessageToSqsQueue(String request) {
        sqs.sendMessage(new SendMessageRequest()
                .withQueueUrl(queueUrl)
                .withMessageBody(request)
                .withDelaySeconds(5));
        return SEND_MESSAGE_TO_OES_SUCCESS;
    }
}