package com.tessco.orderservice.utility;

public interface Constants {
    String SEND_MESSAGE_TO_OES_SUCCESS = "The message was successfully sent to OES";
    String AWS_CONNECTION_ERROR = "Error connecting to Amazon SQS Queue";
    String INVALID_URL = "The queue URL is invalid. ";
    String RUNTIME_ERROR = "A runtime error occurred. Catch all: ";

}
