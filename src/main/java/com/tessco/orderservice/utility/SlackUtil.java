package com.tessco.orderservice.utility;

import com.github.seratch.jslack.Slack;
import com.github.seratch.jslack.api.webhook.Payload;
import com.github.seratch.jslack.api.webhook.WebhookResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

@SuppressWarnings("SpellCheckingInspection")
@Service
public class SlackUtil {

    @Value("${slack-webhook}")
    private String webhook;

    private static final Logger logger = LoggerFactory.getLogger(SlackUtil.class);

    public void sendSlackMessage(String message) {
        Payload payload = Payload.builder().channel("").text(message).build();

        Slack slack = Slack.getInstance();

        logger.debug("webhook from SlackUtil: " + webhook);

        try {
            WebhookResponse response = slack.send(webhook, payload);

            if (response.getCode() != 200) {
                logger.error("Slack Utilities did not work!");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getWebhook() {
        return webhook;
    }

    public void setWebhook(String webhook) {
        this.webhook = webhook;
    }
}
