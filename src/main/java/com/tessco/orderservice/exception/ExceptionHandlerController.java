package com.tessco.orderservice.exception;

import com.amazonaws.services.sqs.model.AmazonSQSException;
import com.amazonaws.services.sqs.model.QueueDoesNotExistException;
import com.tessco.orderservice.utility.SlackUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static com.tessco.orderservice.utility.Constants.*;

@RestControllerAdvice
@SuppressWarnings({"UnusedDeclaration", "unchecked"})
public class ExceptionHandlerController {
    private final static Logger logger = LoggerFactory.getLogger(ExceptionHandlerController.class);
    private SlackUtil slackUtil;

    @ExceptionHandler(QueueDoesNotExistException.class)
    public ResponseEntity queueUrlError(QueueDoesNotExistException e) {
        logger.error((INVALID_URL + e.getMessage()));
        slackUtil.sendSlackMessage(INVALID_URL + e.getMessage());
        return new ResponseEntity(INVALID_URL, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AmazonSQSException.class)
    public ResponseEntity amazonCredentialError(AmazonSQSException e) {
        logger.error((AWS_CONNECTION_ERROR + e.getMessage()));
        slackUtil.sendSlackMessage(AWS_CONNECTION_ERROR + e.getMessage());
        return new ResponseEntity(AWS_CONNECTION_ERROR, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity handleOtherErrors(Exception e) {
        logger.error(RUNTIME_ERROR + e.getMessage());
        slackUtil.sendSlackMessage(RUNTIME_ERROR + e.getMessage());
        return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
